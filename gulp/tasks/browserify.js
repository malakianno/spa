﻿//var fs = require("fs");
var browserify = require('browserify');
var gulp = require('gulp');
var gutil = require('gulp-util');
var watchify = require('watchify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var sourcemaps = require('gulp-sourcemaps');

function CreateBundle() {
	var bundle = transformed.bundle();
	bundle.on('error', function(error) {
		gutil.log(error.message);
	});

	//bundle.pipe(fs.createWriteStream("./scripts/build/js/app.js"));
	bundle
		.pipe(source('app.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./scripts/build/js'));
}

var transformed = browserify('./scripts/app.js', { debug: true, cache: {},
        packageCache: {} })
    .transform('babelify', { presets: ["es2015"] });

transformed = watchify(transformed, {
    delay: 0,
    ignoreWatch: ['**/node_modules/**']
})
.on('update', function () {
	gutil.log('Starting bundle update');
    CreateBundle();
})
.on('time', function (time) { gutil.log('Bundle updated. Time: ' + time + 'ms'); });

CreateBundle();