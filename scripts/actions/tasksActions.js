export const ADD_TASK = 'ADD_TASK'
export const CHANGE_IS_COMPLETED_STATUS_TASK = 'CHANGE_IS_COMPLETED_STATUS_TASK'
export const REMOVE_TASK = 'REMOVE_TASK'
export const CHANGE_FILTER = 'CHANGE_FILTER'

export function addTask(name, deadline) {
	return {
		type: ADD_TASK,
		payload: {
			name: name,
			deadline: deadline
		}
	}
}

export function removeTask(id) {
	return {
		type: REMOVE_TASK,
		payload: {
			id: id
		}
	}
}

export function changeIsCompletedStatus(id) {
	return {
		type: CHANGE_IS_COMPLETED_STATUS_TASK,
		payload: {
			id: id
		}
	}
}

export function changeFilter(filter) {
	return {
		type: CHANGE_FILTER,
		payload: {
			filter: filter
		}
	}
}