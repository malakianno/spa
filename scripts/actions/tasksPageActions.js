export const SAVE_TASK_NAME = 'SAVE_TASK_NAME'
export const SAVE_DEADLINE_DATE = 'SAVE_DEADLINE_DATE'
export const SAVE_NEW_TASK_AREA_VISIBLE_STATUS = 'SAVE_NEW_TASK_AREA_VISIBLE_STATUS'

export function saveTaskName(name) {
	return {
		type: SAVE_TASK_NAME,
		payload: {
			taskName: name
		}
	}
}

export function saveDeadlineDate(date) {
	return {
		type: SAVE_DEADLINE_DATE,
		payload: {
			deadlineDate: date
		}
	}
}

export function saveNewTaskAreaVisibleStatus(isVisible) {
	return {
		type: SAVE_NEW_TASK_AREA_VISIBLE_STATUS,
		payload: {
			isAddTaskAreaVisible: isVisible
		}
	}
}