import { ADD_TASK, CHANGE_IS_COMPLETED_STATUS_TASK, REMOVE_TASK, CHANGE_FILTER } from '../actions/tasksActions'
import FILTERS from '../components/task/tasks-filters'
import TasksHelper from '../helpers/tasksHelper'
import TimeHelper from '../helpers/timeHelper'
import Task from '../components/task/task'

const initialState = {
	day: TimeHelper.getCurrentDate(),
	filter: FILTERS.SHOW_ACTIVE,
	tasksStorage: [
		new Task(0, 'Visit this page', TimeHelper.getCurrentDate(), true),
		new Task(1, 'Refactor this application', TimeHelper.getCurrentDate(), false),
		new Task(2, 'Make a design for this application', TimeHelper.getCurrentDate(), false),
		new Task(3, 'Create something new', TimeHelper.getCurrentDate(), false),
		new Task(4, 'Take over the world', '23.07.2016', false),
		new Task(5, 'Get drunk', '01.01.2016', true),
		new Task(6, 'Get drunk again', '01.01.2017', false)
	]
}

function todos(state = initialState, action) {
	var newState = angular.copy(state);
	switch(action.type) {
		case ADD_TASK:
			newState.tasksStorage.push(new Task(TasksHelper.getNewId(newState.tasksStorage), action.payload.name, action.payload.deadline, false));
			break;
		case CHANGE_IS_COMPLETED_STATUS_TASK:
			newState.tasksStorage = TasksHelper.changeIsCompletedStatusTask(newState.tasksStorage, action.payload.id);
			break;
		case REMOVE_TASK:
			newState.tasksStorage = TasksHelper.removeTask(newState.tasksStorage, action.payload.id);
			break;
		case CHANGE_FILTER:
			newState.filter = action.payload.filter;
			break;
		default: return state;
	}

	return newState;
}

export default todos