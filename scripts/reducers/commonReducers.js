import { SAVE_USERNAME } from '../actions/commonActions'

const initialState = { 
	username: 'guest' 
}

function common(state = initialState, action) {
	var newState = angular.copy(state);
	switch(action.type) {
		case SAVE_USERNAME:
			newState.username = action.payload.username;
			break;
		default:
			return state;
	}

	return newState;
}

export default common