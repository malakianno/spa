import { combineReducers } from 'redux'
import common from './commonReducers'
import todos from './tasksReducers'
import tasksPage from './tasksPageReducers'

const reducers = {
	common,
	todos,
	tasksPage
};

export default combineReducers(reducers);