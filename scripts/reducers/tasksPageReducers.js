import { SAVE_NEW_TASK_AREA_VISIBLE_STATUS, SAVE_TASK_NAME, SAVE_DEADLINE_DATE } from '../actions/tasksPageActions'

const initialState = {
	taskName: '',
	deadlineDate: new Date(),
	isAddTaskAreaVisible: false
}

function tasksPage(state = initialState, action) {
	var newState = angular.copy(state);
	switch(action.type) {
		case SAVE_TASK_NAME:
			newState.taskName = action.payload.taskName;
			break;
		case SAVE_DEADLINE_DATE:
			newState.deadlineDate = action.payload.deadlineDate;
			break;
		case SAVE_NEW_TASK_AREA_VISIBLE_STATUS:
			newState.isAddTaskAreaVisible = action.payload.isAddTaskAreaVisible;
			break;
		default: return state;
	}

	return newState;
}

export default tasksPage;