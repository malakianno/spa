import { mainController } from './controllers/mainController'
import appHeader from './components/appHeader/appHeader.module.js'
import menu from './components/menu/menu.module.js'
import controllersModuleName from './controllers/module'
import config from './app.config'
import ngRedux from 'ng-redux'

// controllers
import './controllers/welcomePageController';

window.app = angular.module('app', [ngRedux, controllersModuleName, menu, appHeader])
	.controller('mainController', mainController)
	.config(config);