class TimeHelper {
	static getSeparator() {
		return '.';
	}

	static getCurrentDate() {
		return this.toDate(new Date());
	}

	static toDate(datetime) {
		return this.dateToFormat(datetime);
	}

	static dateToFormat(date) {
		if(!date) {
			date = new Date();
		}

		var dd = date.getDate();
		var mm = date.getMonth() + 1;
		var yyyy = date.getFullYear();

		if(dd < 10) {
		    dd = '0' + dd;
		} 

		if(mm < 10) {
		    mm = '0'+ mm;
		} 

		return dd + this.getSeparator() + mm + this.getSeparator() + yyyy;
	}
}

export default TimeHelper;