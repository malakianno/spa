mainController.$inject = ['$scope'];

function mainController($scope) {
	var vm = this;

    vm.isAppHeaderVisible = function() {
        return true;
    };
}

export { mainController }