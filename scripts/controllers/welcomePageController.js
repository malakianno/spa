import controllersModuleName from './module'
import { saveUserName } from '../actions/commonActions'
const welcomePageControllerName = 'welcomePageController'

function welcomePageController($scope, $ngRedux) {
	$ngRedux.connect(mapStateToScope)($scope);

	function mapStateToScope(state) {
        return {
            username: state.common.username,
            countTasks: state.todos.tasksStorage.length
        };
    }

    $scope.changeUserName = function() {
    	$ngRedux.dispatch(saveUserName($scope.username));
    }
}

angular.module(controllersModuleName).controller(welcomePageControllerName, ['$scope', '$ngRedux', welcomePageController]);

export default welcomePageControllerName;