import controllersModuleName from './module'
import { addTask, changeIsCompletedStatus, removeTask, changeFilter } from '../actions/tasksActions'
import { saveNewTaskAreaVisibleStatus, saveTaskName, saveDeadlineDate } from '../actions/tasksPageActions'
import FILTERS from '../components/task/tasks-filters'
import TimeHelper from '../helpers/timeHelper'

const tasksPageControllerName = 'tasksPageController'

function tasksPageController($scope, $ngRedux) {
	function configureScope() {
		$scope.dispatchFilter = function() {
			$ngRedux.dispatch(changeFilter($scope.filter));
		}

		$scope.dispatchTaskName = function() {
			$ngRedux.dispatch(saveTaskName($scope.newTaskName));
		}

		$scope.dispatchDeadlineDate = function() {
			$ngRedux.dispatch(saveDeadlineDate($scope.newDeadlineDate));
		}

		$scope.showAddNewTaskArea = function() {
			$ngRedux.dispatch(saveNewTaskAreaVisibleStatus(true));
		}

		$scope.saveTask = function() {
			$ngRedux.dispatch(addTask($scope.newTaskName, TimeHelper.toDate(angular.copy($scope.newDeadlineDate))));
			$ngRedux.dispatch(saveNewTaskAreaVisibleStatus(false));
			resetTaskNameAndDeadline();
		}

		$scope.removeTask = function(id) {
			$ngRedux.dispatch(removeTask(id));
		}

		$scope.completedStatusIsChanged = function(id) {
			$ngRedux.dispatch(changeIsCompletedStatus(id));
		}

		$scope.filters = FILTERS;
	}

	function filterTasks(tasks, filter) {
		const currentDate = TimeHelper.getCurrentDate();
		switch(filter) {
			case FILTERS.SHOW_ALL: 
				return angular.copy(tasks);
			case FILTERS.SHOW_TODAY:
				return angular.copy(tasks).filter(function(task) {
					return task.deadline == currentDate;
				});
			case FILTERS.SHOW_TODAY_ACTIVE:
				return angular.copy(tasks).filter(function(task) {
					return task.deadline == currentDate && !task.isCompleted;
				});
			case FILTERS.SHOW_COMPLETED: 
				return angular.copy(tasks).filter(function(task) {
					return task.isCompleted;
				});
			case FILTERS.SHOW_ACTIVE: 
				return angular.copy(tasks).filter(function(task) {
					return !task.isCompleted;
				});
		}
	}

	function resetTaskNameAndDeadline() {
		$scope.newTaskName = '';
		$scope.dispatchTaskName();
		$scope.newDeadlineDate = new Date();
		$scope.dispatchDeadlineDate();
	}

	function mapStateToScope(state) {
        return {
            userName: state.common.username,
            tasks: filterTasks(state.todos.tasksStorage, state.todos.filter),
            filter: state.todos.filter,
            newTaskName: state.tasksPage.taskName,
            newDeadlineDate: state.tasksPage.deadlineDate,
            isAddNewTaskAreaVisible: state.tasksPage.isAddTaskAreaVisible
        };
    }

    function initialize() {
    	configureScope();
		$ngRedux.connect(mapStateToScope)($scope);
    }

    initialize();
}

angular.module(controllersModuleName).controller(tasksPageControllerName, ['$scope', '$ngRedux', tasksPageController]);

export default tasksPageControllerName;