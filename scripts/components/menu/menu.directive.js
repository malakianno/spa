import menuController from './menu.controller'

function menuDirectiveFactory() {
	var directive = {
		restrict: 'E',
		templateUrl: './scripts/components/menu/templates/menu.html',
		scope: true,
		replace: true,
		controller: menuController
	};

	return directive;
}

export default menuDirectiveFactory;