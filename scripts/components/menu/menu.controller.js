function menuController($scope, $state) {
	$scope.pages = $state.get().filter((state) => angular.isUndefined(state.abstract))
		.map((state) => {
		return {
			name: state.visibleName,
			url: '/#'.concat(state.url)
		};
	});
}

export default menuController;