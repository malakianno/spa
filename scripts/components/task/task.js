 class Task {
	 constructor(id, text, date, isCompleted) {
		this.id = id;
		this.name = text;
		this.deadline = date;
		this.isCompleted = isCompleted;
	}
}

export default Task