import headerDirectiveFactory from './appHeader.directive';

export default angular.module('app.appHeader', [])
    .directive('appHeader', headerDirectiveFactory)
    .name;